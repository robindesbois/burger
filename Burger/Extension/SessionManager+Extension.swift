//
//  SessionManager+Extension.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import Foundation
import Alamofire

extension SessionManager{
    
    static func getManager() -> SessionManager {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "devportal:8443": .disableEvaluation
        ]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        configuration.httpMaximumConnectionsPerHost = 10
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource =  30
        
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        manager.delegate.taskWillPerformHTTPRedirection = nil
        return manager
    }
}
