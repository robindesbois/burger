//
//  String+Extension.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import Foundation

extension String {
    
    func currencyFormatting() -> String {
        if let value = Double(self) {
            let formatter = NumberFormatter()
            formatter.usesGroupingSeparator = true
            formatter.numberStyle = .currency
            formatter.locale = .init(identifier: "fr_FR")
            if let str = formatter.string(for: value) {
                return str
            }
        }
        return ""
    }
}
