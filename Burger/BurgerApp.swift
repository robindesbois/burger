//
//  BurgerApp.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import SwiftUI

@main
struct BurgerApp: App {
    var burgersViewModel = BurgersViewModel()

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(burgersViewModel)
                .withErrorHandling()
        }
    }
}
