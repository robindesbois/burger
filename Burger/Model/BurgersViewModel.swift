//
//  BurgersViewModel.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import Foundation

class BurgersViewModel: ObservableObject {
    @Published var burgerList: [Burger] = []
    @Published var fetching = false
    
    @MainActor
    func fetchData(completion : @escaping (ErrorType)->()) async {
        if burgerList.isEmpty {
            self.fetching = true
            try? await Task.sleep(nanoseconds: 3_000_000_000)
            
            ApiService.shared.fetchData { error, burgerList in
                if let burgerList = burgerList {
                    self.burgerList = burgerList
                } else {
                    completion(error!)
                }
                self.fetching = false
            }
        }
    }
    
    /// Function that takes an amount in cents as a parameter and converts it into euros
    ///
    /// - parameter price: price to convert
    /// - returns: String represents the price in euros
    func convertPrice(price: Int?) -> String {
        return price != nil ?  "\(Double(price!) / 100)".currencyFormatting() : ""
    }
    
    func toggleStatus(for todo: Burger) {
        if let index = burgerList.firstIndex(where: {$0.id == todo.id}) {
            burgerList[index].isSelected =  todo.isSelected
        }
    }
    
    var selectedBurgers: [Burger] {
        get {
            return self.burgerList.filter(  {$0.isSelected == true })
        }
    }
    
    /// Function that calculates the total of the selected burges
    ///
    /// - returns: String represents the total price in euros
    func totalBasket() -> String {
        let totalPrice = selectedBurgers.compactMap{ $0.price }.reduce(0, +)
        return self.convertPrice(price: totalPrice)
    }
}
