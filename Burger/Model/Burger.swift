//
//  Burger.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import Foundation

struct Burger: Identifiable, Decodable {
    var id: String {
        self.reference
    }
    var reference: String
    var title: String
    var description: String
    var imageURL: String
    var price: Int
    var isSelected: Bool?
    
    enum CodingKeys: String, CodingKey {
        case reference = "ref"
        case title
        case description
        case imageURL = "thumbnail"
        case price
        case isSelected
    }
}
