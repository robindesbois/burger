//
//  ValidationError.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import Foundation
import Alamofire

struct ErrorAlert: Identifiable {
    var id = UUID()
    var message: String
    var dismissAction: (() -> Void)?
}

let SERVER_NOT_FOUND = 502
let SERVER_NOT_RESPONDING = 504
let ERROR_SERVER1 = 500
let ERROR_SERVER2 = 503

enum ErrorType: LocalizedError {
    case networkError
    case serverNotResponding
    case errorServer
    case parsingError
    case notfoundServerError
    case unknownError
    
    var errorDescription: String? {
        switch self {
        case .networkError:
            return NSLocalizedString("ERR0R_NETWORK", comment: "")
        case .serverNotResponding:
            return NSLocalizedString("SERVER_NOT_RESPONDING", comment: "")
        case .errorServer:
            return NSLocalizedString("ERR0R_SERVER", comment: "")
        case .parsingError:
            return NSLocalizedString("ERR0R_PARSING", comment: "")
        case .notfoundServerError:
            return NSLocalizedString("SERVER_NOT_FOUND", comment: "")
        case .unknownError:
            return NSLocalizedString("ERROR_UNKNOW", comment: "")
        }
    }
    
    static func getErrorType(forServerError error : Error)-> ErrorType {
        guard NetworkReachabilityManager()!.isReachable else{
            return ErrorType.networkError
        }
        
        if let serverError =  error as? AFError {
            if let  code = serverError.responseCode {
                return getErrorType(fromStatusCode: code)
            }
        }
        return ErrorType.serverNotResponding
    }
    
    static func getErrorType(fromStatusCode code : Int)-> ErrorType {
        switch code {
        case SERVER_NOT_FOUND:
            return ErrorType.notfoundServerError
        case SERVER_NOT_RESPONDING:
            return ErrorType.serverNotResponding
        case ERROR_SERVER1:
            return ErrorType.errorServer
        case ERROR_SERVER2:
            return ErrorType.errorServer
        default:
            return ErrorType.unknownError
        }
    }
}

class ErrorHandling: ObservableObject {
    @Published var currentAlert: ErrorAlert?
    
    func handle(error: Error) {
        currentAlert = ErrorAlert(message: error.localizedDescription)
    }
}

