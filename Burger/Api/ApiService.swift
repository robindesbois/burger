//
//  ApiService.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import Foundation
import Alamofire
import SwiftUI

class ApiService: NSObject {
    private let url = "https://uad.io/bigburger/"
    static let shared = ApiService()
    var cache: NSCache<NSString, UIImage> = NSCache()
    let headers = ["Content-Type": "application/json"]
    var manger: SessionManager!
    private let AUTENTICATE_STATUS_INTERVAL = 200..<499
    
    override init(){
        self.manger = SessionManager.getManager()
    }
    
    /// Function that allows you to retrieve the list of burgers
    ///
    /// - returns: [Burger] representation of the burger list
    func fetchData(completion : @escaping (ErrorType? ,[Burger]?)->()) {
        self.manger.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : self.headers)
            .validate(statusCode: self.AUTENTICATE_STATUS_INTERVAL)
            .responseJSON { response in
                switch response.result {
                case .success:
                    do {
                        let responseContent = try JSONDecoder().decode(BurgerListResponse.self, from: response.data!)
                        guard let burgerList = responseContent.burgers else {
                            return
                        }
                        completion(nil, burgerList)
                    } catch {
                        completion(ErrorType.parsingError, nil)
                    }
                case .failure(let error):
                    completion(ErrorType.getErrorType(forServerError: error), nil)
                }
            }
    }
    
}
