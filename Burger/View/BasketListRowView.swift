//
//  BasketListRowView.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import SwiftUI

struct BasketListRowView: View {
    private let IMAGE_SIZE: CGFloat = 100
    private let CHECKBOX_SIZE: CGFloat = 20
    @EnvironmentObject var burgersViewModel: BurgersViewModel

    @State var burger: Burger
    
    var body: some View {
        HStack {
            AsyncImage(url: URL(string: burger.imageURL), content: { image in
                image.resizable()
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: IMAGE_SIZE, height: IMAGE_SIZE)
            }, placeholder: {
                Image("ic_no_image")
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: IMAGE_SIZE, height: IMAGE_SIZE)
            })
            .frame(width: IMAGE_SIZE, height: IMAGE_SIZE)
            .padding()
            Spacer()
            Text(burgersViewModel.convertPrice(price: burger.price))
                .padding()
        }
        .background(Color("Cell"))
        .overlay(
            RoundedRectangle(cornerRadius: 4).stroke(Color("Border"), lineWidth: 2)
        )
        
    }
}

struct BasketListRowView_Previews: PreviewProvider {
    static var previews: some View {
        let menuItem = Burger(reference: "reference", title: "title", description: "description", imageURL: "none", price: 12, isSelected: false)
        BasketListRowView(burger: menuItem)
    }
}
