//
//  ContentView.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import SwiftUI

struct ContentView: View {
    private let BASKET_BUTTON_SIZE: CGFloat = 35
    private let CORNER_RADUIS: CGFloat = 4
    
    @EnvironmentObject var burgersViewModel: BurgersViewModel
    @EnvironmentObject var errorHandling: ErrorHandling
    
    @State private var showingBasketView = false
    
    var body: some View {
        
        NavigationView {
            ZStack {
                Color("Background")
                    .ignoresSafeArea()
                Color
                    .white
                    .cornerRadius(CORNER_RADUIS)
                    .padding()
                
                NavigationLink(destination: BasketView(), isActive: self.$showingBasketView) {
                    EmptyView()
                } .frame(width: 0, height: 0)
                
                RefreshListView{
                    MenuList(menuItems: burgersViewModel.burgerList)
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                    
                } onRefresh: {
                    Task {
                        await burgersViewModel.fetchData { error in
                            self.errorHandling.handle(error: error)
                        }
                    }
                }.navigationTitle("NAVIGATION_BAR_TITLE")
                    .navigationBarItems(leading: Text("APP_TITLE"))
                    .navigationBarItems(trailing: Button(action:{
                        self.showingBasketView.toggle()
                    }) {
                        Image("ic_basket")
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: BASKET_BUTTON_SIZE, height: BASKET_BUTTON_SIZE)
                    }.buttonStyle(PlainButtonStyle()))
                    .overlay {
                        ProgressView("WAIT_FETCHING")
                            .progressViewStyle(CircularProgressViewStyle(tint: .red)).isHidden(!burgersViewModel.fetching)
                    }
                    .onAppear {
                        Task {
                            await burgersViewModel.fetchData { error in
                                self.errorHandling.handle(error: error)
                            }
                        }
                    }
            }
        }
    }
    
    struct ContentView_Previews: PreviewProvider {
        static var previews: some View {
            ContentView()
                .environmentObject(BurgersViewModel())
        }
    }
}

