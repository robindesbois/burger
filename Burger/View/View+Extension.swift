//
//  View+Extension.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import Foundation
import SwiftUI

extension View {
    
    @ViewBuilder func isHidden(_ isHidden: Bool) -> some View {
        if isHidden {
            self.hidden()
        } else {
            self
        }
    }
    
    func withErrorHandling() -> some View {
        modifier(ErrorHandlerView())
    }
}
