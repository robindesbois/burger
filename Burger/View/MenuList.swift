//
//  MenuList.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import SwiftUI

struct MenuList: View {
    private let PADDING: CGFloat = 16

    var menuItems = [Burger]()
    
    var body: some View {
        
        VStack(alignment: .leading) {
            ForEach(menuItems, id: \.reference) { burger in
                MenuRow(burger: burger)
                    .padding([.leading, .trailing], PADDING)
            }
        }
    }
}

struct MenuList_Previews: PreviewProvider {
    static var previews: some View {
        MenuList()
    }
}
