//
//  BasketList.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import SwiftUI

struct BasketList: View {
    var menuItems = [Burger]()
    
    var body: some View {
        VStack(alignment: .leading) {
            ForEach(menuItems, id: \.reference) { burger in
                BasketListRowView(burger: burger)
                    .padding([.leading, .trailing], 16)
            }
        }
    }
}

struct BasketList_Previews: PreviewProvider {
    static var previews: some View {
        BasketList()
    }
}
