//
//  MenuRow.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import SwiftUI

struct MenuRow: View {
    private let IMAGE_SIZE: CGFloat = 100
    private let CHECKBOX_SIZE: CGFloat = 20
    
    @State var burger: Burger
    @State var checkState = false
    @EnvironmentObject var burgersViewModel: BurgersViewModel
    
    var body: some View {
        
        HStack {
            AsyncImage(url: URL(string: burger.imageURL), content: { image in
                image.resizable()
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: IMAGE_SIZE, height: IMAGE_SIZE)
            }, placeholder: {
                Image("ic_no_image")
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: IMAGE_SIZE, height: IMAGE_SIZE)
            })
            .frame(width: IMAGE_SIZE, height: IMAGE_SIZE)
            .padding()
            
            Text(burgersViewModel.convertPrice(price: burger.price))
                .padding()
            
            Spacer()
            
            Button(action:
                    {
                self.checkState.toggle()
                self.burger.isSelected = self.checkState
                burgersViewModel.toggleStatus(for: self.burger)
            }) {
                Image(checkState ? "ic_checkbox_on" : "ic_checkbox_off")
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: CHECKBOX_SIZE, height: CHECKBOX_SIZE)
            }
            .buttonStyle(PlainButtonStyle())
            .padding()
        }
        .background(Color("Cell"))
        .overlay(
            RoundedRectangle(cornerRadius: 4).stroke(Color("Border"), lineWidth: 2)
        )
    }
}

struct MenuRow_Previews: PreviewProvider {
    static var previews: some View {
        let menuItem = Burger(reference: "reference", title: "title", description: "description", imageURL: "imageURL", price: 12, isSelected: false)
        
        MenuRow(burger: menuItem)
    }
}
