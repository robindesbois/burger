//
//  ErrorHandlerView.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import Foundation
import SwiftUI

struct ErrorHandlerView: ViewModifier {
    @StateObject var errorHandling = ErrorHandling()
    
    func body(content: Content) -> some View {
        content
            .environmentObject(errorHandling)
            .background(
                EmptyView()
                    .alert(item: $errorHandling.currentAlert) { currentAlert in
                        Alert(
                            title: Text("ERROR"),
                            message: Text(currentAlert.message),
                            dismissButton: .default(Text("ACCEPT")) {
                                currentAlert.dismissAction?()
                            }
                        )
                    }
            )
    }
}
