//
//  BasketView.swift
//  Burger
//
//  Created by Regis Alla on 11/09/2022.
//

import SwiftUI

struct BasketView: View {
    @EnvironmentObject var burgersViewModel: BurgersViewModel
    
    var body: some View {
        ZStack {
            Color("Background")
                .ignoresSafeArea()
            Color
                .white
                .cornerRadius(4)
                .padding()
            
            ScrollView {
                BasketList(menuItems: burgersViewModel.selectedBurgers)
            }.navigationTitle("BASKET")
        }.overlay(
            
            VStack {
                HStack {
                    Text("TOTAL \(burgersViewModel.totalBasket())")
                    Spacer()
                    
                    Button("PAY") {
                        
                    }
                    .frame(width: 100)
                    .padding()
                    .background(.green)
                    
                }.frame(maxWidth: .infinity, maxHeight: 50)
                    .padding([.leading], 16)
                    .background(.gray)
            }
            , alignment: .bottom)
    }
}

struct BasketView_Previews: PreviewProvider {
    static var previews: some View {
        BasketView()
    }
}
