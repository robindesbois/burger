//
//  BurgerTests.swift
//  BurgerTests
//
//  Created by Regis Alla on 11/09/2022.
//

import XCTest
@testable import Burger

// Test Structure: Given, When, Then

class BurgerTests: XCTestCase {
    
    // Test on selected burgers
    func testBurgersViewModel_selectedBurgres() {
        // Given
       let burgersViewModel = BurgersViewModel()
        let burgerOne = Burger(reference: "BestOne", title: "is best", description: "big big big", imageURL: "imageURL", price: 910)
        let burgerTwo = Burger(reference: "BestTwo", title: "is best", description: "big big big", imageURL: "imageURL", price: 312, isSelected: true)
        let burgerThree = Burger(reference: "BestThree", title: "is best", description: "big big big", imageURL: "imageURL", price: 422)

        // When
        burgersViewModel.burgerList.append(burgerOne)
        burgersViewModel.burgerList.append(burgerTwo)
        burgersViewModel.burgerList.append(burgerThree)
        
        //Then
        XCTAssertEqual(burgersViewModel.selectedBurgers.count, 1)
        XCTAssertNotEqual(burgersViewModel.selectedBurgers.count, 0)
        XCTAssertNotEqual(burgersViewModel.selectedBurgers.count, 3)
        XCTAssertNotNil(burgersViewModel.selectedBurgers)
    }
    
    // Euro price conversion test
    func testBurgersViewModel_convertPrice() {
        // Given
       let burgersViewModel = BurgersViewModel()
        
        // When
        let price = 312
        let priceTwo = 410
        
        //Then
        XCTAssertEqual(burgersViewModel.convertPrice(price: price), "3,12 €")
        XCTAssertNotEqual(burgersViewModel.convertPrice(price: price), "3,1 €")
        XCTAssertNotNil(burgersViewModel.convertPrice(price: price))

        XCTAssertEqual(burgersViewModel.convertPrice(price: priceTwo), "4,10 €")
        XCTAssertNotEqual(burgersViewModel.convertPrice(price: priceTwo), "4,1 €")
        XCTAssertNotNil(burgersViewModel.convertPrice(price: priceTwo))
    }
    
    // Basket total price test
    func testBurgersViewModel_totalBasket() {
        // Given
       let burgersViewModel = BurgersViewModel()
        let burgerOne = Burger(reference: "BestOne", title: "is best BestOne", description: "big big big", imageURL: "imageURL", price: 910)
        let burgerTwo = Burger(reference: "BestTwo", title: "is best BestTwo", description: "big big big", imageURL: "imageURL", price: 312)
        let burgerThree = Burger(reference: "BestThree", title: "is best BestThree", description: "big big big", imageURL: "imageURL", price: 422)
        let burgerFour = Burger(reference: "BestFour", title: "is best BestFour", description: "big big big", imageURL: "imageURL", price: 420, isSelected: true)
        let burgerFive = Burger(reference: "BestFive", title: "is best BestFive", description: "big big big", imageURL: "imageURL", price: 50, isSelected: true)

        // When
        burgersViewModel.burgerList.append(burgerOne)
        burgersViewModel.burgerList.append(burgerTwo)
        burgersViewModel.burgerList.append(burgerThree)
        burgersViewModel.burgerList.append(burgerFour)
        burgersViewModel.burgerList.append(burgerFive)

        
        //Then
        XCTAssertEqual(burgersViewModel.totalBasket(), "4,70 €")
        XCTAssertNotEqual(burgersViewModel.totalBasket(), "4,7 €")
        XCTAssertNotEqual(burgersViewModel.totalBasket(), "4,700 €")
        XCTAssertNotEqual(burgersViewModel.totalBasket(), "0,470 €")
        XCTAssertNotNil(burgersViewModel.totalBasket())
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
